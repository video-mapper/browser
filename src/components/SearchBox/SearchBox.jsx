import React from 'react';
import IonRangeSlider from 'react-ion-slider';
import './SearchBox.scss';

export default function SearchBox({
  tags = [],
  className = 'search-box',
  defaultText = '按標籤篩選內容',
  hourFrom = 0,
  hourTo = 24,
  filterVideoByTag = () => {},
  filterVideoByTimeRange = () => {},
  ...props
}) {
  return (
    <div className={className} {...props}>
      <select
        onChange={(evt) => {
          filterVideoByTag(evt.target.value);
        }}
      >
        <option value="">{`-- ${defaultText} --`}</option>
        {tags.map((tag) => (
          <option key={tag} value={tag}>
            {tag}
          </option>
        ))}
      </select>
      <IonRangeSlider
        type="double"
        grid
        min={0}
        max={24}
        from={hourFrom}
        to={hourTo}
        postfix=":00"
        onChange={({ from, to }) => filterVideoByTimeRange(from, to)}
      />
    </div>
  );
}
