/**
 * This file handles the react-redux state to props conversion
 * for the components. Expecting these components to be used
 * within a react-redux `Provider` component.
 *
 * The variable `state` here is the state object of the whole
 * app. All components currently share the same object that
 * describes the data loaded according to user's intention
 * and usage.
 */

import { connect } from 'react-redux';
import PureSearchBox from './SearchBox';
import { filterVideoByTag, filterVideoByTimeRange } from '../../reducers/videosReducer';

export const SearchBox = connect(
  // mapStateToProps
  ({ videos: { filtered: videos, filters } }) => ({ videos, filters }),
  // mapDispatchToProps
  (dispatch) => ({
    filterVideoByTag: (tag) => (dispatch(filterVideoByTag(tag))),
    filterVideoByTimeRange: (from, to) => (dispatch(filterVideoByTimeRange(from, to))),
  }),
)(PureSearchBox);

export default PureSearchBox;
