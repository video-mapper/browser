import React from 'react';
import Modal from 'react-responsive-modal';
import EventMap from '../EventMap/EventMap';
import { EventMarker } from '../EventMarker';
import { VideoEventList } from '../EventList';
import { VideoPlayer } from '../VideoPlayer';
import './VideoPopup.scss';

/*
const googleMapSymbolPath = {
  CIRCLE: 0,
  FORWARD_CLOSED_ARROW: 1,
  FORWARD_OPEN_ARROW: 2,
  BACKWARD_CLOSED_ARROW: 3,
  BACKWARD_OPEN_ARROW: 4,
}

const playerState = {
  ENDED: 0,
  PLAYING: 1,
  PAUSED: 2,
};
*/

export default function VideoPopup({
  video = undefined,
  events = [],
  clearHighlight = () => {},
  highlightEvent = () => {},
}) {
  if (typeof video === 'undefined') {
    return null;
  }
  const { vendorId } = video;
  let ended = false;
  return (
    <Modal
      className="video-popup"
      open={typeof vendorId !== 'undefined'}
      onClose={() => {
        ended = true;
        clearHighlight();
      }}
      center
    >
      <VideoPlayer
        containerClassName="video-popup--player-wrapper"
        className="video-popup--player-wrapper--youtube-player"
        hasEnded={() => (ended)}
      />
      <EventMap
        id="video-popup--popup-map"
        events={events}
        EventMarker={EventMarker}
        onSelectMarker={({ event }) => highlightEvent(event, video)}
        center={{
          // need to update this according to current user browsing
          lat: 22.357983,
          lng: 114.126296,
        }}
      />
      <VideoEventList
        onClick={(evt, event) => highlightEvent(event, video)}
      />
    </Modal>
  );
}
