import React from 'react';
import './SlidingPane.scss';

export default function SlidingPane({className='', toggleText=() => ('Toggle'), bottom=false, children=null}) {
  let containerRef = React.createRef();
  let contentsRef = React.createRef();

  const classNames = [
    'sliding-pane',
    bottom ? 'bottom' : 'top',
  ];
  if (className !== '') {
    classNames.push(className);
  }

  const onToggle = bottom
    // toggleBottom
    ? () => {
      const { offsetTop, offsetHeight, offsetParent } = containerRef.current;
      const { height: parentHeight } = offsetParent.getBoundingClientRect();
      const { borderBottomWidth: parentBorderBottomCss } = window.getComputedStyle(offsetParent);
      const parentBorderBottom = parseInt(parentBorderBottomCss.slice(0, -2))
      const offsetBottom = parentHeight - parentBorderBottom - offsetTop - offsetHeight;
      if (offsetBottom === 0) {
        containerRef.current.style.bottom = `-${contentsRef.current.getBoundingClientRect().height}px`;
        return;
      }
      containerRef.current.style.bottom = 0;
    }
    // toggleTop
    : () => {
      const { offsetTop, offsetParent } = containerRef.current;
      const { borderTopWidth: parentBorderTopCss } = window.getComputedStyle(offsetParent);
      const parentBorderTop = parseInt(parentBorderTopCss.slice(0, -2))
      if (offsetTop - parentBorderTop === 0) {
        containerRef.current.style.top = `-${contentsRef.current.getBoundingClientRect().height}px`;
        return;
      }
      containerRef.current.style.top = 0;
    };

  return (
    <div ref={containerRef} className={classNames.join(' ')}>
      <button type="button" className="sliding-pane--toggle" onClick={onToggle}>{toggleText()}</button>
      <div ref={contentsRef} className="sliding-pane--contents">{children}</div>
    </div>
  );
}
