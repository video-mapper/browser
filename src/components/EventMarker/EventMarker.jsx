import React from 'react';
import { Marker } from '@react-google-maps/api';

const googleMapAnimations = {
  BOUNCE: 1,
  DROP: 2,
};

export default function EventMarker({ current, event, ...props }) {
  return event.start <= current && event.end > current ? (
    <Marker {...props} zIndex={2} animation={googleMapAnimations.BOUNCE} />
  ) : (
    <Marker {...props} zIndex={1} />
  );
}
