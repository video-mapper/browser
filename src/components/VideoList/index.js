/**
 * This file handles the react-redux state to props conversion
 * for the components. Expecting these components to be used
 * within a react-redux `Provider` component.
 *
 * The variable `state` here is the state object of the whole
 * app. All components currently share the same object that
 * describes the data loaded according to user's intention
 * and usage.
 */

import { connect } from 'react-redux';
import PureVideoList from './VideoList';

export const VideoList = connect(
  // mapStateToProps
  ({ videos: { filtered: videos } }) => ({ videos }),
)(PureVideoList);

export default PureVideoList;
