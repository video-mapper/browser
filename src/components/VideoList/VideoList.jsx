import React from 'react';
import './VideoList.scss';

function clickVideo(onClick, video) {
  return (evt) => {
    evt.preventDefault();
    return onClick(evt, video);
  };
}

export default function VideoList({ videos, onClick = () => {} }) {
  return (
    <ul className="videos">
      {videos.map((video) => (
        <li
          className="videos--video"
          key={`${video.vendor}--${video.vendorId}`}
        >
          <a
            className="videos--video--link"
            href={`https://www.youtube.com/watch?v=${video.vendorId}`}
            target="_blank"
            rel="noopener noreferrer"
            onClick={clickVideo(onClick, video)}
          >
            <img
              className="videos--video--thumbnail"
              alt="Video thumbnail"
              src={`https://img.youtube.com/vi/${video.vendorId}/default.jpg`}
            />
            <div className="videos--video--title">{video.title}</div>
          </a>
        </li>
      ))}
    </ul>
  );
}
