/**
 * This file handles the react-redux state to props conversion
 * for the components. Expecting these components to be used
 * within a react-redux `Provider` component.
 *
 * The variable `state` here is the state object of the whole
 * app. All components currently share the same object that
 * describes the data loaded according to user's intention
 * and usage.
 */

import { connect } from 'react-redux';
import PureVideoPlayer from './VideoPlayer';
import { clearHighlight, highlightEvent } from '../../reducers/highlightReducer';
import { clearPlayerState, updatePlayerState } from '../../reducers/playerStateReducer';

export const VideoPlayer = connect(
  // mapStateToProps
  ({ highlight: { event, video } }) => {
    if (typeof video === 'undefined') {
      return {};
    }
    const { events } = video;
    return {
      video,
      event,
      events,
      start: event && event.start ? event.start : 0,
    };
  },
  // mapDispatchToProps
  (dispatch) => ({
    clearHighlight: () => (dispatch(clearHighlight())),
    clearPlayerState: () => (dispatch(clearPlayerState())),
    highlightEvent: (event, video) => dispatch(highlightEvent(event, video)),
    updatePlayerState: (current) => (dispatch(updatePlayerState(current))),
  }),
)(PureVideoPlayer);

export default PureVideoPlayer;
