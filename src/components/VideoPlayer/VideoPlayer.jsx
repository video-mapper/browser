import React from 'react';
import YouTube from 'react-youtube';

const playerState = {
  ENDED: 0,
  PLAYING: 1,
  PAUSED: 2,
};

export default function VideoPlayer({
  video,
  containerClassName,
  onReady,
  className,
  height = 390,
  width = 640,
  start = 0,
  autoplay = 1,
  hasEnded = () => (false),
  clearPlayerState = () => {},
  updatePlayerState = () => {},
}) {
  let playing = false;
  const { vendorId } = video;
  return (
    <YouTube
      containerClassName={containerClassName}
      className={className}
      videoId={vendorId}
      opts={{
        height,
        width,
        playerVars: {
          start,
          autoplay,
        },
      }}
      onReady={onReady}
      onStateChange={(evt) => {
        const player = evt.target;
        if (evt.data === playerState.PLAYING) {
          playing = setInterval(() => {
            const state = player.getPlayerState();
            if (
              playing !== false
              && (state === playerState.ENDED || state === playerState.PAUSED || hasEnded())
            ) {
              clearInterval(playing);
              clearPlayerState();
              playing = false; // reset playing function ref
              return;
            }
            console.log('youtube player current state', state);
            updatePlayerState(player.getCurrentTime());
          }, 1000);
        }
      }}
    />
  );
}
