import React from 'react';
import './Sidebar.scss';

export const PureSidebar = ({
  className = '',
  open = true,
  canClose = false,
  onClose = () => {},
  children = [],
  ...props
}) => {
  const effectiveClassName = open
    ? `sidebar ${className}`
    : `sidebar ${className} hidden`;
  const actionBar = canClose ? (
    <div className="sidebar--actions">
      <button
        className="sidebar--actions--close"
        type="button"
        onClick={onClose}
      >
        x
      </button>
    </div>
  ) : null;
  return (
    <div className={effectiveClassName} {...props}>
      {actionBar}
      {children}
    </div>
  );
};

export default PureSidebar;
