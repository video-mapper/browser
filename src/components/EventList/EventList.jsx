import React from 'react';
import './EventList.scss';

function clickEvent(onClick, event, video) {
  return (evt) => {
    evt.preventDefault();
    return onClick(evt, event, video);
  };
}

export default function EventList({ events = [], onClick = () => {} }) {
  return (
    <ul className="events">
      {events.map(({ event, video }, key) => (
        <li
          className="events--event"
          key={`${video.vendor}--${video.vendorId}--${key}`}
        >
          <a
            className="events--event--link"
            href={`https://www.youtube.com/watch?v=${video.vendorId}`}
            target="_blank"
            rel="noopener noreferrer"
            onClick={clickEvent(onClick, event, video)}
          >
            <img
              className="events--event--thumbnail"
              alt="Video thumbnail"
              src={`https://img.youtube.com/vi/${video.vendorId}/default.jpg`}
            />
            <div className="events--event--details">
              <div className="events--event--details--desc">{event.desc}</div>
              <div className="events--event--details--video-title">
                {`影片︰${video.title}`}
              </div>
              <div className="events--event--details--time">
                {`時段︰第 ${event.start} 秒至 ${event.end} 秒`}
              </div>
            </div>
          </a>
        </li>
      ))}
    </ul>
  );
}
