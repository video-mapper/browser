/**
 * This file handles the react-redux state to props conversion
 * for the components. Expecting these components to be used
 * within a react-redux `Provider` component.
 *
 * The variable `state` here is the state object of the whole
 * app. All components currently share the same object that
 * describes the data loaded according to user's intention
 * and usage.
 */

import { connect } from 'react-redux';
import PureEventList from './EventList';
import { highlightEvent } from '../../reducers/highlightReducer';

export const EventList = connect(
  // mapStateToProps
  ({ videos: { filtered: videos } }) => {
    const events = videos.reduce((accu, video) => {
      const videoEvents = video.events.map((event) => ({ event, video }));
      accu.push(...videoEvents);
      return accu;
    }, []);
    return { events };
  },
)(PureEventList);

export const VideoEventList = connect(
  // mapStateToProps
  ({ highlight: { video } }) => {
    if (typeof video === 'undefined') {
      return {};
    }
    return {
      events: video.events.map((event) => ({ event, video })),
    };
  },
  // mapStateToProps
  (dispatch) => ({
    highlightEvent: (event, video) => dispatch(highlightEvent(event, video)),
  }),
)(PureEventList);

export default PureEventList;
