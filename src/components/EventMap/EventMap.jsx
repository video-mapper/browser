import React from 'react';
import { GoogleMap, Marker } from '@react-google-maps/api';

const EventMap = ({
  center,
  events = [],
  zoom = 14,
  width = '100%',
  height = '100vh',
  options = {},
  EventMarker = Marker,
  mapEventMarkerProps = () => ({}),
  onSelectMarker = () => {},
  ...props
}) => (
  <GoogleMap
    center={center}
    zoom={zoom}
    width={width}
    height={height}
    options={options}
    {...props}
  >
    {events.map((event, index) => {
      const { lat, lng } = event;
      return (
        <EventMarker
          key={index}
          event={event}
          position={{ lat, lng }}
          onClick={() => onSelectMarker({ event })}
          mapEventMarkerProps={mapEventMarkerProps}
        />
      );
    })}
  </GoogleMap>
);

export default EventMap;
