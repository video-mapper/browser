/**
 * This file collects all the components to export.
 * to simplify path finding efforts.
 */

import { EventList, VideoEventList } from './EventList';
import { EventMap } from './EventMap';
import { EventMarker } from './EventMarker';
import { SearchBox } from './SearchBox';
import { PureSlidingPane as SlidingPane } from './SlidingPane';
import { PureSidebar as Sidebar } from './Sidebar';
import { VideoList } from './VideoList';
import { VideoPlayer } from './VideoPlayer';
import { VideoPopup } from './VideoPopup';

export {
  EventList,
  EventMap,
  EventMarker,
  SearchBox,
  Sidebar,
  SlidingPane,
  VideoEventList,
  VideoList,
  VideoPlayer,
  VideoPopup,
};

export default {
  EventList,
  EventMap,
  EventMarker,
  SearchBox,
  Sidebar,
  SlidingPane,
  VideoEventList,
  VideoList,
  VideoPlayer,
  VideoPopup,
};
