import React from 'react';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import { LoadScript } from '@react-google-maps/api';
import {
  EventMap,
  SearchBox,
  Sidebar,
  SlidingPane,
  VideoList,
  VideoPopup,
} from './components';
import highlightReducer, {
  highlightVideo,
  defaultState as highlight,
} from './reducers/highlightReducer';
import videosReducer from './reducers/videosReducer';
import playerStateReducer from './reducers/playerStateReducer';
import all from './videos.json';
import './App.scss';

const store = createStore(
  combineReducers({
    videos: videosReducer,
    highlight: highlightReducer,
    playerState: playerStateReducer,
  }),
  {
    videos: { all, filtered: all, filters: {} },
    highlight,
    playerState: { current: 0 },
  },
  // eslint-disable-next-line no-underscore-dangle, no-undef
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

const googleMapsApiKey = process.env.REACT_APP_GOOGLE_API_KEY || '';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <LoadScript id="script-loader" googleMapsApiKey={googleMapsApiKey}>
          <EventMap
            id="map"
            center={{
              lat: 22.357983,
              lng: 114.126296,
            }}
            options={{
              fullscreenControlOptions: {
                position: 9, // google.maps.ControlPosition.RIGHT_BOTTOM
              },
              mapTypeControlOptions: {
                style: 2, // google.maps.MapTypeControlStyle.DROPDOWN_MENU
                position: 7, // google.maps.ControlPosition.RIGHT_TOP
              },
            }}
          />
          <Sidebar>
            <SlidingPane>
              <SearchBox tags={['打人', '放人', '警察增援', '立場姐姐']} />
            </SlidingPane>
            <VideoList
              videos={all}
              onClick={(evt, video) => store.dispatch(highlightVideo(video))}
            />
          </Sidebar>
          <VideoPopup />
        </LoadScript>
      </Provider>
    </div>
  );
}

export default App;
