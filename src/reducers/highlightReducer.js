export const defaultState = {};

export default function highlightReducer(state = defaultState, action = {}) {
  switch (action.type) {
    case 'HIGHLIGHT_NONE':
      return { ...defaultState };
    case 'HIGHLIGHT_VIDEO':
      return { ...state, video: action.video };
    case 'HIGHLIGHT_EVENT':
      return {
        ...state,
        video: action.video,
        event: action.event,
      };
    default:
      return state;
  }
}

export const clearHighlight = () => ({
  type: 'HIGHLIGHT_NONE',
});

export const highlightVideo = (video) => ({
  type: 'HIGHLIGHT_VIDEO',
  video,
});

export const highlightEvent = (event, video) => ({
  type: 'HIGHLIGHT_EVENT',
  event,
  video,
});
