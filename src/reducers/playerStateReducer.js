const defaultState = {
  current: 0,
};

export default function playerStateReducer(state = defaultState, action = {}) {
  switch (action.type) {
    case 'PLAYER_STATE_RESET': {
      return { ...defaultState };
    }
    case 'PLAYER_STATE_UPDATE': {
      const { current } = action;
      return { current };
    }
    default: {
      return state;
    }
  }
}

const clearPlayerState = () => ({
  type: 'PLAYER_STATE_RESET',
});

const updatePlayerState = (current) => ({
  type: 'PLAYER_STATE_UPDATE',
  current,
});

export {
  clearPlayerState,
  updatePlayerState,
};
