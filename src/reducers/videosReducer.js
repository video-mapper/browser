/**
 * Filtering logic.
 *
 * Returns a filtered list of video according to the filters
 * given.
 *
 * @param Object[] all
 *   Array of all video objects available.
 * @param Object filters
 *   The key-value of search criteria.
 */
const filtering = (all, filters) => {
  if (Object.keys(filters).length === 0) {
    return all;
  }
  return all.filter((video) => {
    if (typeof filters.from !== 'undefined' && filters.from > video.hour) {
      return false;
    }
    if (typeof filters.to !== 'undefined' && filters.to < video.hour) {
      return false;
    }
    const tags = video.events.reduce((accu, curr) => {
      accu.push(...curr.tags);
      return accu;
    }, []);
    if (typeof filters.tag !== 'undefined' && tags.indexOf(filters.tag) === -1) {
      return false;
    }
    return true;
  }).map((video) => {
    const { events } = video;
    if (typeof filters.tag === 'undefined') {
      return video;
    }
    // leave only filtered events in the video.
    return { ...video, events: events.filter((event) => (event.tags.indexOf(filters.tag) !== -1)) };
  });
};

/**
 * Main reducer.
 *
 * @param Object state
 * @param Object action
 */
export default function videoReducer(
  state = { all: [], filtered: [], filters: {} },
  action,
) {
  switch (action.type) {
    case 'VIDEO_FILTER_TAG':
    {
      const { tag } = action;
      const newFilters = { ...state.filters, tag };
      if (newFilters.tag === '') {
        delete newFilters.tag;
      }
      return {
        ...state,
        filters: newFilters,
        filtered: filtering(state.all, newFilters),
      };
    }
    case 'VIDEO_FILTER_TIME_RANGE':
    {
      const { from, to } = action;
      const newFilters = { ...state.filters, from, to };
      if (newFilters.from === 0) {
        delete newFilters.from;
      }
      if (newFilters.to === 24) {
        delete newFilters.to;
      }
      return {
        ...state,
        filters: newFilters,
        filtered: filtering(state.all, newFilters),
      };
    }
    default:
      return state;
  }
}

//
// Actions function below. Used for dispatch.
//

/**
 * Search videos with event of a certain tag.
 *
 * @param string tag
 *   A tag for search.
 */
const filterVideoByTag = (tag) => ({
  type: 'VIDEO_FILTER_TAG',
  tag,
});

/**
 * Search videos with video.hour in a certain time range.
 *
 * @param string from
 *   The numeric hour (0-24).
 * @param string to
 *   The numeric hour (0-24).
 */
const filterVideoByTimeRange = (from, to) => ({
  type: 'VIDEO_FILTER_TIME_RANGE',
  from,
  to,
});

export {
  filterVideoByTag,
  filterVideoByTimeRange,
};
