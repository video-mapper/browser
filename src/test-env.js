/**
 * A file to setup jest environment
 */
import $ from 'jquery';

global.$ = global.jQuery = $;
