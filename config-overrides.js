/* eslint import/no-extraneous-dependencies: warn, no-unused-vars: warn, no-param-reassign: warn */
const webpack = require('webpack');

module.exports = {
  webpack: (config, env) => {
    if (!config.plugins) {
      config.plugins = [];
    }

    config.plugins.push(
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
    );

    return config;
  },
  jest: (config) => {
    return Object.assign({}, config, {
      verbose: true,
      setupFiles: ['./src/test-env.js']
    });
  },
};
